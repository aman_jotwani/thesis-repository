@echo off

rem if not defined DevEnvDir call "w:\src\buildshell.bat"
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set INCLUDE=w:\libs\imgui\;%INCLUDE%
set INCLUDE=w:\libs\stb\;%INCLUDE%
set INCLUDE=C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include;%INCLUDE%

set LIB=w:\libs\imgui\build;%LIB%
set LIB=C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Lib\x64;%LIB%

rem -Od means suppress code movement, simplifies the debugging process apparently.
rem -Oi means generate intrinsic functions
rem -fp refers to floating point ops, except- means suppress exceptions.
set DebugFlags=-Od -MTd -nologo -Gm- -GR- -fp:fast -fp:except- -EHa- -EHs -Zo -FC -Z7 -W4 -WX -Oi
set ReleaseFlags=-MTd -nologo -Gm- -GR- -EHa- -EHs
rem This is for pcg, unary minus applied to unsigned type
set PcgIgnoreWarning=-wd4146
set ImguiIgnoreWarning=-wd4800
set IgnoreWarnings=%PcgIgnoreWarning% %ImguiIgnoreWarning% -wd4100 -wd4201 -wd4838 -wd4244 -wd4005 -wd4189 -wd4715 -wd4505 -wd4267
set LinkerFlags=-incremental:no user32.lib d3d11.lib d3dx11.lib d3dcompiler.lib imgui.lib /NODEFAULTLIB:LIBCMT
rem I ignore double to float warnings. How badly can that end?

mkdir w:\build
pushd w:\build
cl %DebugFlags% %IgnoreWarnings% -Fm:euphoria.map -Fe:Euphoria.exe w:\src\main.cpp /link %LinkerFlags%
popd