cbuffer ConstBuffer {
    matrix Projection;
};

struct VS_INPUT
{
    float3 Pos : POSITION;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
};

PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output;
    output.Pos = mul(Projection, float4(input.Pos, 1.0));
    return output;
}

float4 PS(PS_INPUT input) : SV_TARGET
{
    return float4(1.0, 0.0, 0.0, 1.0);
}    
    
