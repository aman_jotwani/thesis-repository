Texture2D diffuse_map : register(t0);
Texture2D alpha_map : register(t1);
SamplerState linear_sampler : register(s0); 

cbuffer ConstantBuffer1 : register(b0) {
    matrix Projection;
};

cbuffer ConstantBuffer2 : register(b1) {
    matrix Model;
};

struct VS_INPUT
{
    float3 Pos : POSITION;
    float3 TexCoord : TEXCOORD;
    float3 Normal : NORMAL;
    float3 Tangent : TANGENT;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float3 TexCoord : TEXCOORD;
};

PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output;
    output.Pos = mul(Projection, float4(input.Pos, 1.0));
    output.TexCoord = input.TexCoord;
    // flip uv's
    output.TexCoord.y = 1 - input.TexCoord.y;
    return output;
}

float4 PS(PS_INPUT input) : SV_TARGET
{
    float4 diffuse =  diffuse_map.Sample(linear_sampler, float2(input.TexCoord.xy));
    return diffuse;
    // if(alpha_map) {
    //     float4 alpha = alpha_map.Sample(linear_sampler, float2(input.TexCoord.xy));
    //     return float4(diffuse*alpha);
    // } else {
    //     return diffuse;
    // }
}