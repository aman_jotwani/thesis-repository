#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

internal const int gTempStrLength = 128;
struct Vertex {
    Vector3 position;
    Vector3 texCoord;
    Vector3 normal;
    Vector3 tangent;
};

struct Texture {
    ID3D11ShaderResourceView* ptr;
    int width;
    int height;
};

struct Material {
    char name[gTempStrLength];
    Texture diffuseMap;
    Texture specularMap;
    Texture ambientMap;
    Texture alphaMap;
    Vector3 ambientCoeff;
    Vector3 diffuseCoeff;
    Vector3 specularCoeff;
};

struct Draw_Cmd {
    int startIdx;
    int numIdxs;
    int matIdx;
};

struct Draw_Cmd_List {
    Array<Vertex> vtxBuf;
    Array<u32> idxBuf;
    Array<Material> matList;
    Array<Draw_Cmd> cmdList;
    ID3D11Buffer* d3dVtxBuf;
    ID3D11Buffer* d3dIdxBuf;
};

struct Vertex_Constant_Buffer {
    Mat4 projection;
};

struct RenderCmd {
    u32 vtx_stride;
    u32 vtx_count;
    ID3D11Buffer* vtx_buf;
    ID3D11VertexShader* vtx_shader;
    ID3D11InputLayout* vtx_layout;
    ID3D11PixelShader* pix_shader;
    ID3D11RasterizerState* rasterizer_state;
};

void SetupD3D11Device() {
    HRESULT hr = S_OK;

    RECT rc;
    GetClientRect(globals.windowHandle, &rc);
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] = {
        D3D_DRIVER_TYPE_HARDWARE,
    };
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] = {
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    // back buffer creation
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = globals.windowHandle;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = globals.windowed;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++) {
        D3D_DRIVER_TYPE driver_type = driverTypes[driverTypeIndex];
        D3D_FEATURE_LEVEL f;
        hr = D3D11CreateDeviceAndSwapChain(NULL, driver_type, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
                                           D3D11_SDK_VERSION, &sd, &globals.swapChain, &globals.device, &f,
                                           &globals.deviceContext);
    }

    ASSERT(!FAILED(hr));

    ImGui_ImplDX11_Init(globals.windowHandle, globals.device, globals.deviceContext);

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = NULL;
    hr = globals.swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
    if (FAILED(hr)) {
        // todo: log and crash.
    }

    hr = globals.device->CreateRenderTargetView(pBackBuffer, NULL, &globals.renderTargetView);
    pBackBuffer->Release();
    if (FAILED(hr)) {
        // todo: log and crash.
    }

    // Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory(&descDepth, sizeof(descDepth));
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    // descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.Format = DXGI_FORMAT_D32_FLOAT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    ID3D11Texture2D* depthStencil;
    hr = globals.device->CreateTexture2D(&descDepth, NULL, &depthStencil);
    if (FAILED(hr)) {
        // todo: logging.
    }

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory(&descDSV, sizeof(descDSV));
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = globals.device->CreateDepthStencilView(depthStencil, &descDSV, &globals.depthStencilView);
    if (FAILED(hr)) {
        // todo: logging
    }

    globals.deviceContext->OMSetRenderTargets(1, &globals.renderTargetView, globals.depthStencilView);

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    // vp.TopLeftY = globals.screen_height; // origin is bottom left corner
    globals.deviceContext->RSSetViewports(1, &vp);

    globals.screenWidth = width;
    globals.screenHeight = height;

    globals.deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void LoadStaticVertexBuffer(ID3D11Buffer** buffer, ID3D11Device* device, Array<Vertex> vtx_buffer) {
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = vtx_buffer.size * sizeof(Vertex);
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    D3D11_SUBRESOURCE_DATA init_data;
    ZeroMemory(&init_data, sizeof(init_data));
    init_data.pSysMem = vtx_buffer.data;
    HRESULT result = device->CreateBuffer(&bd, &init_data, buffer);
    ASSERT(!FAILED(result));
}

void CreateVertexBuffer(ID3D11Buffer** buffer, void* vtx_data, int vtx_count, int vtx_size, ID3D11Device* device) {
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = vtx_count * vtx_size;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    D3D11_SUBRESOURCE_DATA init_data;
    ZeroMemory(&init_data, sizeof(init_data));
    init_data.pSysMem = vtx_data;
    HRESULT result = device->CreateBuffer(&bd, &init_data, buffer);
    ASSERT(!FAILED(result));
}

void CreateIndexBuffer(ID3D11Buffer** buffer, void* idx_data, int idx_count, ID3D11Device* device) {
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = idx_count * sizeof(uint32_t);
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    D3D11_SUBRESOURCE_DATA init_data;
    ZeroMemory(&init_data, sizeof(init_data));
    init_data.pSysMem = idx_data;
    HRESULT result = device->CreateBuffer(&bd, &init_data, buffer);
    ASSERT(!FAILED(result));
}

void LoadStaticIndexBuffer(ID3D11Buffer** buffer, ID3D11Device* device, Array<u32> idx_buffer) {
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.ByteWidth = idx_buffer.size * sizeof(u32);
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    D3D11_SUBRESOURCE_DATA init_data;
    ZeroMemory(&init_data, sizeof(init_data));
    init_data.pSysMem = idx_buffer.data;
    HRESULT result = globals.device->CreateBuffer(&bd, &init_data, buffer);
    ASSERT(!FAILED(result));
}

//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
internal HRESULT CompileShaderFromFile(LPCSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel,
                                       ID3DBlob** ppBlobOut) {
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows
    // the shaders to be optimized and to run exactly the way they will run in
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DX11CompileFromFile(szFileName, NULL, NULL, szEntryPoint, szShaderModel, dwShaderFlags, 0, NULL, ppBlobOut,
                               &pErrorBlob, NULL);
    if (FAILED(hr)) {
        if (pErrorBlob != NULL)
            OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
        if (pErrorBlob)
            pErrorBlob->Release();
        return hr;
    }
    if (pErrorBlob)
        pErrorBlob->Release();

    return S_OK;
}

void LoadVertexShader(ID3D11VertexShader** vert_shader, ID3D11InputLayout** vert_layout,
                      D3D11_INPUT_ELEMENT_DESC* layout, int num_descs, ID3D11Device* device, char* filename) {
    ID3DBlob* blob = NULL;
    HRESULT result = CompileShaderFromFile(filename, "VS", "vs_4_0", &blob);
    result = device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, vert_shader);
    ASSERT(!FAILED(result));

    result = device->CreateInputLayout(layout, num_descs, blob->GetBufferPointer(), blob->GetBufferSize(), vert_layout);
    ASSERT(!FAILED(result));

    blob->Release();
}

void LoadPixelShader(ID3D11PixelShader** pixel_shader, ID3D11Device* device, char* filename) {
    ID3DBlob* blob = NULL;
    HRESULT result = CompileShaderFromFile(filename, "PS", "ps_4_0", &blob);
    result = globals.device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, pixel_shader);
    ASSERT(!FAILED(result));
    blob->Release();
}

void LoadStaticConstBuffer(ID3D11Buffer** buf, ID3D11Device* device) {
    D3D11_BUFFER_DESC bd;
    bd.ByteWidth = sizeof(Vertex_Constant_Buffer);
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    bd.MiscFlags = 0;
    HRESULT result = device->CreateBuffer(&bd, NULL, buf);
    ASSERT(!FAILED(result));
}

void LoadRGBATexture(Texture* tex, char* filepath, ID3D11Device* device) {
    int components;
    u8* pixel_data = stbi_load(filepath, &tex->width, &tex->height, &components, 4);
    if (pixel_data) {
        D3D11_TEXTURE2D_DESC desc;
        desc.Width = tex->width;
        desc.Height = tex->height;
        desc.MipLevels = 1;
        desc.ArraySize = 1;
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.SampleDesc.Count = 1;
        desc.SampleDesc.Quality = 0;
        desc.Usage = D3D11_USAGE_DEFAULT;
        desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        desc.CPUAccessFlags = desc.MiscFlags = 0;

        D3D11_SUBRESOURCE_DATA init_data;
        init_data.pSysMem = pixel_data;
        init_data.SysMemPitch = tex->width * 4;

        ID3D11Texture2D* resource = NULL;
        HRESULT hr = device->CreateTexture2D(&desc, &init_data, &resource);
        if (SUCCEEDED(hr) && resource) {
            D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
            ZeroMemory(&srv_desc, sizeof(srv_desc));
            srv_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
            srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
            srv_desc.Texture2D.MipLevels = 1;
            hr = device->CreateShaderResourceView(resource, &srv_desc, &tex->ptr);
            ASSERT(SUCCEEDED(hr));
        }
    } else {
        vs_debug_log("While loading %s, stb_image: %s", filepath, stbi_failure_reason());
    }
}

void CreateRasterizerState(ID3D11RasterizerState** state, D3D11_FILL_MODE fill_mode = D3D11_FILL_SOLID) {
    D3D11_RASTERIZER_DESC rd;
    ZeroMemory(&rd, sizeof(rd));
    rd.FillMode = fill_mode;
    rd.CullMode = D3D11_CULL_BACK;
    rd.FrontCounterClockwise = TRUE;
    rd.DepthClipEnable = TRUE;
    rd.ScissorEnable = FALSE;
    rd.MultisampleEnable = FALSE;
    rd.AntialiasedLineEnable = FALSE;

    HRESULT hr = globals.device->CreateRasterizerState(&rd, state);
    ASSERT(!FAILED(hr));
}

void RunPipeline(RenderCmd rc) {
    // INPUT ASSEMBLER
    UINT offset = 0;
    globals.deviceContext->IASetVertexBuffers(0, 1, &rc.vtx_buf, &rc.vtx_stride, &offset);
    globals.deviceContext->IASetInputLayout(rc.vtx_layout);

    // VERTEX SHADER
    globals.deviceContext->VSSetShader(rc.vtx_shader, NULL, 0);

    // RASTERIZER
    globals.deviceContext->RSSetState(rc.rasterizer_state);

    // PIXEL SHADER
    globals.deviceContext->PSSetShader(rc.pix_shader, NULL, 0);

    // DRAW COMMANDS
    globals.deviceContext->Draw(rc.vtx_count, 0);
}