
bool has_cycle(Node* head) {
    if(head->next == nullptr) return(false); // single node
    else if(head->next->next == nullptr) return(false); // two nodes
    else
    {
        Node* curr = head;
        Node* ahead = head->next->next;
        if(curr == ahead) return(true); // two nodes pointing to each other
        else
        {
            while(curr != ahead)
            {
                curr = curr->next;
                if(ahead->next == nullptr) return(false);
                else ahead = ahead->next;
            }
            return(true);
        }
    }
 }