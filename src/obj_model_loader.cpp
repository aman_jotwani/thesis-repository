/* TODO: 
1) DONE. create new draw command if material changes within the same group.
2) DONE. NEEDS TESTING. let the CRT in, remove msvc specific functions
*/

internal char* GetAbsolutePath(char* dir, char* rel_path) {
    char* buffer = new char[128];
    strncpy(buffer, dir, 128);
    strncat(buffer, rel_path, 128);
    return buffer;
}

internal Array<Material> ReadMtlFile(char* directory, char* relative_path) {
    char* filepath = GetAbsolutePath(directory, relative_path);
    File_Handle handle = Win32ReadEntireFile(filepath);
    delete filepath;
    if(handle.Contents) {
        char* file = (char*)handle.Contents;
        char* end_of_file = file + handle.ContentSize;
        char line[128];

        Array<Material> result;
        Material current_material = {};
        while(file != end_of_file) {
            { // fill line
                int char_index = 0;
                ZeroMemory(line, sizeof(line));
                while (*file != '\n') {
                    line[char_index++] = *(file++);
                }
                line[char_index] = '\n';
                while(*file == '\n') { file++; } // skip till next line
                while(*file == ' ' || *file == '\t') { file++; } // skip trailing whitespace
            }

            switch(line[0]) {
                case 'n': {
                    static bool first = true;
                    if(first) first = false;
                    else {
                        result.push_back(current_material);
                        current_material = {};
                    }
                    // NOTE: depends on TempStrLength 
                    sscanf(line, "newmtl %128s", current_material.name);
                } break;

                case 'K': {
                    switch(line[1]) {
                        case 'a': {
                            sscanf(line, "Ka %f %f %f",
                                   &current_material.ambientCoeff.x,
                                   &current_material.ambientCoeff.y,
                                   &current_material.ambientCoeff.z);
                        } break;

                        case 'd': {
                            sscanf(line, "Kd %f %f %f",
                                   &current_material.diffuseCoeff.x,
                                   &current_material.diffuseCoeff.y,
                                   &current_material.diffuseCoeff.z);
                        } break;

                        case 's': {
                            sscanf(line, "Ks %f %f %f",
                                   &current_material.specularCoeff.x,
                                   &current_material.specularCoeff.y,
                                   &current_material.specularCoeff.z);
                        } break;
                    }
                } break;

                case 'm': {
                    char relative_texture_path[128];
                    char* absolute_texture_path;
                    if(line[4] == 'd') {
                        sscanf(line, "map_d %128s", relative_texture_path);
                        absolute_texture_path = GetAbsolutePath(directory, relative_texture_path);
                        LoadRGBATexture(&current_material.alphaMap, absolute_texture_path, globals.device);
                    }
                    switch(line[5]) {
                        case 'd': {
                            sscanf(line, "map_Kd %128s", relative_texture_path);
                            absolute_texture_path = GetAbsolutePath(directory, relative_texture_path);
                            LoadRGBATexture(&current_material.diffuseMap, absolute_texture_path, globals.device);
                        } break;
                        case 's': {
                            sscanf(line, "map_Ks %128s", relative_texture_path);
                            absolute_texture_path = GetAbsolutePath(directory, relative_texture_path);
                            LoadRGBATexture(&current_material.specularMap, absolute_texture_path, globals.device);
                        } break;
                        case 'a': {
                            sscanf(line, "map_Ka %128s", relative_texture_path);
                            absolute_texture_path = GetAbsolutePath(directory, relative_texture_path);
                            LoadRGBATexture(&current_material.ambientMap, absolute_texture_path, globals.device);
                        } break;
                    }
                }
            }
        }

        result.push_back(current_material);
        return result;
    }
}

internal int GetNumStrLength(int num) {
    int count = 0;
    if (num < 0) {
        num *= -1;
        count = 1;
    }
    while (num > 0) {
        num /= 10;
        count++;
    }
    return count;
}

internal int GetArrayIndex(int parsed_index, int curr_buffer_size) {
    return (parsed_index < 0) ? curr_buffer_size + parsed_index : parsed_index - 1;
}

internal Vector3 ReadVertexAttribute(char** str, Array<Vector3> arr) {
    int i;
    sscanf(*str, "%d", &i);
    Vector3 result = arr[GetArrayIndex(i, arr.size)];
    *str += GetNumStrLength(i);
    return result;
}

// parses a string of the form:
// p/uv/n, p//n, p/uv
// and returns the number of bytes parsed
internal int ParseVertex(Vertex* vtx, Array<Vector3> positions, Array<Vector3> tex_coords, Array<Vector3> normals,
                              char* vtx_str)
{
    if (*vtx_str == 0 || *vtx_str == '\r' || *vtx_str == '\n')
        return 0;
    char* ptr = vtx_str;
    vtx->position = ReadVertexAttribute(&ptr, positions);
    if (*ptr == '/') {
        if(*(++ptr) == '/') { // p//n
            ptr += 1;          // skip the '/'
            vtx->normal = ReadVertexAttribute(&ptr, normals);
        } else { // p/uv..
            vtx->texCoord = ReadVertexAttribute(&ptr, tex_coords);
            if (*ptr == '/') { // p/uv/n
                ptr += 1;      // skip the '/'
                vtx->normal = ReadVertexAttribute(&ptr, normals);
            }
        }
    }
    ptrdiff_t result = ptr - vtx_str;
    return (int)result;
}

internal Array<u32> CreateIndices(u32 vtx_bufsize, int vtxs_added) {
    Array<u32> idxs;
    idxs.push_back(vtx_bufsize);
    idxs.push_back(vtx_bufsize + 1);
    idxs.push_back(vtx_bufsize + 2);

    switch(vtxs_added) {
        case 4: {
            idxs.push_back(vtx_bufsize + 2);
            idxs.push_back(vtx_bufsize + 3);
            idxs.push_back(vtx_bufsize);
        } break;

        case 5: {
            vs_debug_log("You need to modify CreateIndices to handle 5 vtxs.");
        } break;
        case 6: {
            vs_debug_log("You need to modify CreateIndices to handle 6 vtxs.");
        } break;
        case 7: {
            vs_debug_log("You need to modify CreateIndices to handle 7 vtxs.");
        } break;
    }
    return idxs;
}


Draw_Cmd_List LoadObjModel(char* directory, char* relative_path) {
    char* filepath = GetAbsolutePath(directory, relative_path);
    File_Handle f = Win32ReadEntireFile(filepath);
    delete filepath;

    if (f.Contents) {
        Draw_Cmd_List result;
        Draw_Cmd curr_dc = {0, 0, -1};
        Array<Vector3> positions, tex_coords, normals;

        char* file = (char*)f.Contents;
        char* end_of_file = file + f.ContentSize;
        char line[128];

        while (file != end_of_file) { // the condition may be skipped, incase file becomes end_of_file+1,etc, use ptrdiff_t?
            // fill line
            // NOTE: there's no need to fill line buffer for comments and other useless lines
            {
                int char_index = 0;
                ZeroMemory(line, sizeof(line));
                while (*file != '\n') {
                    line[char_index++] = *(file++);
                }
                line[char_index] = '\n';
                while(*file == '\n') {file++;} // skip till next line
                while(*file == ' ' || *file == '\t') { file++; } // skip trailing whitespace
            }

            switch (line[0]) {
                case 'v': {
                    switch (line[1]) {
                        case 't': {
                            Vector3 uv;
                            int floats_read = sscanf(line, "vt %f %f %f", &uv.x, &uv.y, &uv.z);
                            if (floats_read != 3) {
                                sscanf(line, "vt %f %f", &uv.x, &uv.y);
                            }
                            tex_coords.push_back(uv);
                        } break;

                        case 'n': {
                            Vector3 n;
                            int floats_read = sscanf(line, "vn %f %f %f", &n.x, &n.y, &n.z);
                            normals.push_back(n);
                        } break;

                        default: {
                            Vector3 p;
                            sscanf(line, "v %f %f %f", &p.x, &p.y, &p.z);
                            positions.push_back(p);
                        }
                    }
                } break;

                case 'f': {
                    Array<Vertex> vtxs;
                    char* bytes = (char*)line;
                    bool done = false;
                    while (!done) {
                        while (*(++bytes) == ' ') {
                        }
                        Vertex vtx;
                        int bytes_parsed = ParseVertex( &vtx, positions, tex_coords, normals, bytes);
                        if (bytes_parsed) {
                            vtxs.push_back(vtx);
                            bytes += bytes_parsed;
                        } else
                            done = true;
                    }

                    Array<u32> idxs = CreateIndices(result.vtxBuf.size, vtxs.size);
                    result.vtxBuf.extend(vtxs);
                    result.idxBuf.extend(idxs);

                    curr_dc.numIdxs += idxs.size;

                    vtxs.free();
                    idxs.free();
                } break;

                case 'u': {
                    static bool first = true;
                    if(first) first = false;
                    else {
                        result.cmdList.push_back(curr_dc);
                        curr_dc.startIdx = result.idxBuf.size;
                        curr_dc.numIdxs = 0;
                        curr_dc.matIdx = -1;
                    }
                    char mtl_name[128];
                    sscanf(line, "usemtl %128s", mtl_name);
                    for(int index = 0; index < result.matList.size; ++index) {
                        if(strncmp(result.matList[index].name, mtl_name, 128) == 0) {
                            curr_dc.matIdx = index;
                            break;
                        }
                    }
                } break;

                case 'g': {
                } break;

                case 'm': {
                    char mtl_fname[128];
                    sscanf(line, "mtllib %128s", mtl_fname);
                    result.matList = ReadMtlFile(directory, mtl_fname);
                } break;
            }
        }

        result.cmdList.push_back(curr_dc);
        return result;
    } else {
        // vs_debug_log("Unable to open file.");
    }
}
