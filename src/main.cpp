/*
                            Euphoria Graphics Engine -- v0.1

TODO:
1) ENABLE RELATIVE MOUSE MOVEMENT.
2) Camera bugs
3) Transparency

Conventions I use:
1) Origin is top left corner.
2) Matrices are always column major, in code and in shaders.
3) Projection matrix is left handed, z goes into the screen.
4) PascalCase for types, functions, lower_case for variables
*/

#define _CRT_SECURE_NO_WARNINGS
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int32_t s32;
typedef int16_t s16;
typedef float f32;
typedef double f64;

// internal functions are supposed to be used in their files only
#define internal static
#define ASSERT(Exp)                                                                                                    \
    if (!(Exp)) {                                                                                                      \
        *(int*)0 = 0;                                                                                                  \
    }

#include "../libs/imgui/examples/directx11_example/imgui_impl_dx11.h"

#include "../libs/imgui/examples/directx11_example/imgui_impl_dx11.cpp"

#include "math/vector.cpp"

#include "math/matrix.cpp"

#include "array.h"

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3dx11.h>
#include <windows.h>

struct FpsCamera {
    f32 yawDegrees;
    f32 pitchDegrees;
    s16 prevMouseX;
    s16 prevMouseY;
    Vector3 position;
    Vector3 forward;
    Vector3 right;
    f32 speed;
    bool firstTime;
};

struct G {
    HWND windowHandle;
    ID3D11Device* device;
    ID3D11DeviceContext* deviceContext;
    IDXGISwapChain* swapChain;
    ID3D11RenderTargetView* renderTargetView;
    ID3D11DepthStencilView* depthStencilView;
    ID3D11RasterizerState* rasterizerState;

    // display settings
    int screenWidth;
    int screenHeight;
    bool windowed;

    FpsCamera cam;
} globals;

#include "windows_platform.cpp"

#include "d3d11_wrapper.cpp"

#include "obj_model_loader.cpp"

internal Mat4 ComputeViewTransform() {
    Mat4 t = Translation(-globals.cam.position.x, -globals.cam.position.y, -globals.cam.position.z);
    Mat4 m = CreateFromColumns(globals.cam.right, CrossProduct(globals.cam.right, globals.cam.forward),
                               globals.cam.forward, Vector3(0, 0, 0));
    return MultiplyMat4(Transpose(m), t);
}

void InitCamera() {
    globals.cam.speed = 10.f;
    globals.cam.yawDegrees = 0.0f;
    globals.cam.pitchDegrees = 0.0f;
    globals.cam.prevMouseX = globals.cam.prevMouseY = 0;
    globals.cam.position.Set(0, 1.0f, -1.0f);
    globals.cam.right.Set(1, 0, 0);
    globals.cam.forward.Set(0, 0, 1);
    globals.cam.firstTime = true;
}

#define VOXELIZER_IMPLEMENTATION
#include "../reference_src_code/voxelizer-master/voxelizer.h"

int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd_line, int cmd_show) {
    globals.windowed = true;
    globals.screenWidth = 1920;
    globals.screenHeight = 1080;

    InitCamera();

    SetupWindow(instance, globals.screenWidth, globals.screenHeight, cmd_show);
    SetupD3D11Device();

    // PipelineState {
    //     Buffer* vbo
    //     Buffer* ibo
    //     InputLayout*
    //     VertexShader*
    //     PixelShader*
    //     SamplerState*
    //     RasterizerState*
    //     BlendState*
    //     Buffer* const_buffers[]
    //     DrawCmdList -- start_idx, num_idxs and material_index
    // } only thing that changes while rendering is ShaderResources(textures)
    // load shaders
    // ID3D11InputLayout* vert_layout = NULL;
    // ID3D11VertexShader* vert_shader = NULL;
    // ID3D11PixelShader* pixel_shader = NULL;
    // ID3D11Buffer* obj_constant_buffer = NULL;
    // {
    //     char* shader_filename = "../src/shaders/simple.fx";
    //     D3D11_INPUT_ELEMENT_DESC layout[] = {
    //         {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
    //         {"TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT,
    //         D3D11_INPUT_PER_VERTEX_DATA, 0},
    //         {"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,
    //         0},
    //         {"TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,
    //         0}
    //     };
    //     LoadVertexShader(&vert_shader, &vert_layout, layout, ARRAYSIZE(layout),
    //                      globals.device, shader_filename);
    //     globals.deviceContext->IASetInputLayout(vert_layout);
    //     LoadPixelShader(&pixel_shader, globals.device, shader_filename);

    //     LoadStaticConstBuffer(&obj_constant_buffer, globals.device);
    // }

    // Draw_Cmd_List render_list = LoadObjModel("w:/assets/models/crytek-sponza/", "sponza.obj");
    // {
    //     LoadStaticVertexBuffer(&render_list.d3dVtxBuf, globals.device, render_list.vtxBuf);
    //     LoadStaticIndexBuffer(&render_list.d3dIdxBuf, globals.device, render_list.idxBuf);
    // }

    // Draw_Cmd_List banner_mesh = LoadObjModel("w:/assets/models/crytek-sponza/", "banner.obj");
    // {
    //     LoadStaticVertexBuffer(&banner_mesh.d3dVtxBuf, globals.device, banner_mesh.vtxBuf);
    //     LoadStaticIndexBuffer(&banner_mesh.d3dIdxBuf, globals.device, banner_mesh.idxBuf);
    // }

    ID3D11SamplerState* sampler;
    {
        D3D11_SAMPLER_DESC samp_desc;
        ZeroMemory(&samp_desc, sizeof(samp_desc));
        // samp_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
        samp_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
        samp_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
        samp_desc.MinLOD = 0;
        samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
        HRESULT hr = globals.device->CreateSamplerState(&samp_desc, &sampler);
        ASSERT(!FAILED(hr));
    }

    vx_mesh_t* mesh;
    vx_mesh_t* result;

    // TODO: supply from obj loader
    Vector3 vertices[] = {Vector3(0.0f, 0.0f, 0.2f), Vector3(1.0f, 0.0f, 0.2f), Vector3(0.0f, 1.0f, 0.2f)};
    u32 indices[] = {0, 1, 2};
    mesh = vx_mesh_alloc(ARRAYSIZE(vertices), ARRAYSIZE(indices));

    for (size_t i = 0; i < ARRAYSIZE(indices); ++i) {
        mesh->indices[i] = indices[i];
        mesh->vertices[i].x = vertices[i].x;
        mesh->vertices[i].y = vertices[i].y;
        mesh->vertices[i].z = vertices[i].z;
    }

    result = vx_voxelize(mesh, 0.03, 0.03, 0.03, 0.01);

    D3D11_INPUT_ELEMENT_DESC layout[] = {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0}};
    ID3D11Buffer* voxel_vbuffer;
    ID3D11Buffer* voxel_ibuffer;
    ID3D11Buffer* voxel_const_buffer;
    ID3D11VertexShader* voxel_vshader;
    ID3D11PixelShader* voxel_pshader;
    ID3D11InputLayout* voxel_vertex_layout;
    ID3D11RasterizerState* raster_state;
    CreateVertexBuffer(&voxel_vbuffer, result->vertices, (int)result->nvertices, sizeof(vx_vertex_t), globals.device);
    CreateIndexBuffer(&voxel_ibuffer, result->indices, (int)result->nindices, globals.device);
    LoadStaticConstBuffer(&voxel_const_buffer, globals.device);
    LoadVertexShader(&voxel_vshader, &voxel_vertex_layout, layout, ARRAYSIZE(layout), globals.device,
                     "../src/shaders/voxel.fx");
    LoadPixelShader(&voxel_pshader, globals.device, "../src/shaders/voxel.fx");
    CreateRasterizerState(&raster_state, D3D11_FILL_WIREFRAME);

    // RenderCmd rc;
    // {
    //     struct SimpleVertex {
    //         Vector3 position;
    //         Vector3 color;
    //     };

    //     SimpleVertex vertices[] = {{Vector3(0.0f, 0.0f, 0.2f), Vector3(1.0f, 0.0f, 0.0f)},
    //                                {Vector3(1.0f, 0.0f, 0.2f), Vector3(1.0f, 0.0f, 0.0f)},
    //                                {Vector3(0.0f, 1.0f, 0.3f), Vector3(1.0f, 0.0f, 0.0f)}};

    //     rc.vtx_count = ARRAYSIZE(vertices);
    //     rc.vtx_stride = sizeof(SimpleVertex);
    //     CreateVertexBuffer(&rc.vtx_buf, (void*)vertices, rc.vtx_count, rc.vtx_stride, globals.device);
    //     D3D11_INPUT_ELEMENT_DESC input_layout_desc[] = {
    //         {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
    //         {"COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,
    //         0}};
    //     LoadVertexShader(&rc.vtx_shader, &rc.vtx_layout, input_layout_desc, ARRAYSIZE(input_layout_desc),
    //                      globals.device, "../src/shaders/voxel.fx");
    //     LoadPixelShader(&rc.pix_shader, globals.device, "../src/shaders/voxel.fx");
    //     CreateRasterizerState(&rc.rasterizer_state);
    // }

    f32 fovy = 45.0f;
    MSG msg = {0};
    while (msg.message != WM_QUIT) {
        msg = {0};
        while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        ImGui_ImplDX11_NewFrame();

        {
            Mat4 modelview_transform = ComputeViewTransform();
            Vertex_Constant_Buffer cb;
            f32 z_near = 0.1f;
            f32 z_far = 5000.0f;
            cb.projection =
                D3DPerspectiveProjectionLH(z_near, z_far, fovy, (f32)globals.screenWidth / (f32)globals.screenHeight);
            cb.projection = MultiplyMat4(cb.projection, modelview_transform);
            globals.deviceContext->UpdateSubresource(voxel_const_buffer, 0, NULL, &cb, 0, 0);
        }

        ImGui::DragFloat("Vertical field of view (degrees)", &fovy);
        ImGui::DragFloat("Camera speed", &globals.cam.speed);
        f32 arr[3] = {globals.cam.forward.x, globals.cam.forward.y, globals.cam.forward.z};
        ImGui::DragFloat3("Cam forward", arr);
        globals.cam.forward.Set(arr[0], arr[1], arr[2]);
        float ClearColor[4] = {0.0f, 0.125f, 0.3f, 1.0f};
        globals.deviceContext->ClearRenderTargetView(globals.renderTargetView, ClearColor);

        //
        // Clear the depth buffer to 1.0 (max depth)
        //
        globals.deviceContext->ClearDepthStencilView(globals.depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
        ImGui::Text("Vertices: %d", result->nvertices);
        ImGui::Text("Indices: %d", result->nindices);

        UINT offset = 0;
        UINT stride = sizeof(vx_vertex_t);
        globals.deviceContext->IASetVertexBuffers(0, 1, &voxel_vbuffer, &stride, &offset);
        globals.deviceContext->IASetIndexBuffer(voxel_ibuffer, DXGI_FORMAT_R32_UINT, 0);
        globals.deviceContext->IASetInputLayout(voxel_vertex_layout);
        globals.deviceContext->VSSetShader(voxel_vshader, NULL, 0);
        globals.deviceContext->VSSetConstantBuffers(0, 1, &voxel_const_buffer);
        globals.deviceContext->PSSetShader(voxel_pshader, NULL, 0);
        globals.deviceContext->RSSetState(raster_state);
        globals.deviceContext->DrawIndexed(result->nindices, 0, 0);
        // RunPipeline(rc);
        // globals.deviceContext->VSSetShader(vert_shader, NULL, 0);
        // globals.deviceContext->VSSetConstantBuffers(0, 1, &obj_constant_buffer);
        // globals.deviceContext->PSSetShader(pixel_shader, NULL, 0);
        // globals.deviceContext->PSSetSamplers( 0, 1, &sampler );

        // UINT stride = sizeof(Vertex);
        // UINT offset = 0;
        // globals.deviceContext->IASetVertexBuffers(0, 1, &render_list.d3dVtxBuf, &stride, &offset);
        // globals.deviceContext->IASetIndexBuffer(render_list.d3dIdxBuf, DXGI_FORMAT_R32_UINT, 0);
        // for(int i = 0; i < render_list.cmdList.size; ++i) {
        //     Draw_Cmd dc = render_list.cmdList[i];
        //     Material mat;
        //     if(dc.matIdx >= 0) {
        //         mat = render_list.matList[dc.matIdx];
        //         if(mat.diffuseMap.ptr) {
        //             globals.deviceContext->PSSetShaderResources(0, 1, &mat.diffuseMap.ptr);
        //         }
        //         if(mat.alphaMap.ptr) {
        //             globals.deviceContext->PSSetShaderResources(1, 1, &mat.alphaMap.ptr);
        //         }
        //     }
        //     globals.deviceContext->DrawIndexed(dc.numIdxs, dc.startIdx, 0);
        // }

        // globals.deviceContext->IASetVertexBuffers(0, 1, &banner_mesh.d3dVtxBuf, &stride, &offset);
        // globals.deviceContext->IASetIndexBuffer(banner_mesh.d3dIdxBuf, DXGI_FORMAT_R32_UINT, 0);
        // for(int i = 0; i < banner_mesh.cmdList.size; ++i) {
        //     Draw_Cmd dc = banner_mesh.cmdList[i];
        //     Material mat;
        //     if(dc.matIdx >= 0) {
        //         mat = banner_mesh.matList[dc.matIdx];
        //         if(mat.diffuseMap.ptr) {
        //             globals.deviceContext->PSSetShaderResources(0, 1, &mat.diffuseMap.ptr);
        //         }
        //         if(mat.alphaMap.ptr) {
        //             globals.deviceContext->PSSetShaderResources(1, 1, &mat.alphaMap.ptr);
        //         }
        //     }
        //     globals.deviceContext->DrawIndexed(dc.numIdxs, dc.startIdx, 0);
        // }

        ImGui::Render();
        globals.swapChain->Present(1, 0);
    }
    return (0);
}
