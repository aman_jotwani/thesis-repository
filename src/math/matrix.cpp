#include <immintrin.h>
#include <math.h>

#ifdef _WIN32
#define Align(Alignment) __declspec(align((Alignment)))
#elif (__APPLE__ && __MACH__)
#define Align(Alignment) __attribute__((aligned((Alignment))))
#endif

#define SIMD_ALIGN Align(16)

static double PI = 3.1415;
double ToRadians(float x) {
    return (x * (PI / 180.0));
}
double ToDegrees(double x) {
    return ( x * (180.0 / PI));
}

struct Mat4 {
    SIMD_ALIGN float ColumnMajor[16];
    Mat4() {
        for (int i = 0; i < 16; ++i) {
            ColumnMajor[i] = 0.0f;
        }
    }
};

Mat4 IdentityMatrix() {
    Mat4 Result;
    Result.ColumnMajor[0] = 1.0f;
    Result.ColumnMajor[5] = 1.0f;
    Result.ColumnMajor[10] = 1.0f;
    Result.ColumnMajor[15] = 1.0f;
    return (Result);
}

Mat4 CreateFromColumns(const Vector3& col1, const Vector3& col2, const Vector3& col3, const Vector3& col4)
{
    Mat4 Result;
    Result.ColumnMajor[0] = col1.x;
    Result.ColumnMajor[1] = col1.y;
    Result.ColumnMajor[2] = col1.z;

    Result.ColumnMajor[4] = col2.x;
    Result.ColumnMajor[5] = col2.y;
    Result.ColumnMajor[6] = col2.z;

    Result.ColumnMajor[8] = col3.x;
    Result.ColumnMajor[9] = col3.y;
    Result.ColumnMajor[10] = col3.z;

    Result.ColumnMajor[12] = col4.x;
    Result.ColumnMajor[13] = col4.y;
    Result.ColumnMajor[14] = col4.z;

    Result.ColumnMajor[15] = 1.0f;

    return(Result);
}

Mat4 Transpose(const Mat4& other) {
    Mat4 result;
    int row_num = 0;
    int k = row_num;
    for (int i = 0; i < 16; i++) {
        result.ColumnMajor[i] = other.ColumnMajor[k];
        k += 4;
        if (k >= 16) {
            k = ++row_num;
        }
    }
    return (result);
}

Mat4 StripTranslation(Mat4 other) {
    Mat4 result = other;
    result.ColumnMajor[12] = 0.0f;
    result.ColumnMajor[13] = 0.0f;
    result.ColumnMajor[14] = 0.0f;
    return (result);
}

Mat4 Translation(float x, float y, float z) {
    Mat4 result;
    result.ColumnMajor[0] = 1.0f;
    result.ColumnMajor[5] = 1.0f;
    result.ColumnMajor[10] = 1.0f;
    result.ColumnMajor[12] = x;
    result.ColumnMajor[13] = y;
    result.ColumnMajor[14] = z;
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

Mat4 Scaling(float x, float y, float z) {
    Mat4 result;
    result.ColumnMajor[0] = x;
    result.ColumnMajor[5] = y;
    result.ColumnMajor[10] = z;
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

Mat4 RotationX(float AngleInDegrees) {
    double AngleInRadians = ToRadians(AngleInDegrees);
    Mat4 result;
    result.ColumnMajor[0] = 1.0f;
    result.ColumnMajor[5] = cos(AngleInRadians);
    result.ColumnMajor[6] = sin(AngleInRadians);
    result.ColumnMajor[9] = -sin(AngleInRadians);
    result.ColumnMajor[10] = cos(AngleInRadians);
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

Mat4 RotationY(float AngleInDegrees) {
    double AngleInRadians = ToRadians(AngleInDegrees);
    Mat4 result;
    result.ColumnMajor[0] = cos(AngleInRadians);
    result.ColumnMajor[2] = sin(AngleInRadians);
    result.ColumnMajor[5] = 1.0f;
    result.ColumnMajor[8] = -sin(AngleInRadians);
    result.ColumnMajor[10] = cos(AngleInRadians);
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

Mat4 RotationZ(float AngleInDegrees) {
    double AngleInRadians = ToRadians(AngleInDegrees);
    Mat4 result;
    result.ColumnMajor[0] = cos(AngleInRadians);
    result.ColumnMajor[1] = sin(AngleInRadians);
    result.ColumnMajor[4] = -sin(AngleInRadians);
    result.ColumnMajor[5] = cos(AngleInRadians);
    result.ColumnMajor[10] = 1.0f;
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

// maps x:[0, Width], y:[0, Height] and z:[Near, Far] to OpenGL ndc unit cube
Mat4 OrthoProjectionOffCenterLH(float Near, float Far, float Width, float Height) {
    Mat4 result;
    result.ColumnMajor[0] = 2 / Width;
    result.ColumnMajor[5] = 2 / Height;
    result.ColumnMajor[10] = 2 / (Far - Near);
    result.ColumnMajor[12] = -1.0f;
    result.ColumnMajor[13] = -1.0f;
    result.ColumnMajor[14] = -(Far + Near) / (Far - Near);
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
Mat4 OrthoProjectionLH(float Near, float Far, float Width, float Height) {
    Mat4 result;
    result.ColumnMajor[0] = 2 / Width;
    result.ColumnMajor[5] = 2 / Height;
    result.ColumnMajor[10] = 2 / (Far - Near);
    result.ColumnMajor[14] = -(Far + Near) / (Far - Near);
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
Mat4 OrthoProjectionRH(float Near, float Far, float Width, float Height) {
    Mat4 result;
    result.ColumnMajor[0] = 2 / Width;
    result.ColumnMajor[5] = 2 / Height;
    result.ColumnMajor[10] = -2 / (Far - Near);
    result.ColumnMajor[14] = -(Far + Near) / (Far - Near);
    result.ColumnMajor[15] = 1.0f;
    return (result);
}

Mat4 PerspProjectionLH(float Near, float Far, float Fovy, float AspectRatio) {
    Mat4 result;
    float tan_part = tan(ToRadians(Fovy / 2));
    result.ColumnMajor[0] = 1.0f / (tan_part * AspectRatio);
    result.ColumnMajor[5] = 1.0f / tan_part;
    result.ColumnMajor[10] = (Far + Near) / (Far - Near);
    result.ColumnMajor[11] = 1.0f;
    result.ColumnMajor[14] = -2.0f * Near * Far / (Far - Near);
    return (result);
}

Mat4 PerspProjectionRH(float Near, float Far, float Fovy, float AspectRatio) {
    Mat4 result;
    float tan_part = tan(ToRadians(Fovy / 2));
    result.ColumnMajor[0] = 1.0f / (tan_part * AspectRatio);
    result.ColumnMajor[5] = 1.0f / tan_part;
    result.ColumnMajor[10] = -(Far + Near) / (Far - Near);
    result.ColumnMajor[11] = -1.0f;
    result.ColumnMajor[14] = -2.0f * Near * Far / (Far - Near);
    return (result);
}

Mat4 D3DOrthoProjectionLH(f32 near, f32 far, f32 width, f32 height) {
    Mat4 result;
    return result;
}

// maps x:[-width/2, width/2], y:[-height/2, height/2] and z:[near, far] to D3D
// clip space
Mat4 D3DPerspectiveProjectionLH(f32 z_near, f32 z_far, f32 fovy, f32 aspect_ratio) {
    Mat4 result;
    f32 tan_part = tan(ToRadians(fovy / 2));
    result.ColumnMajor[0] = 1.0f / (tan_part * aspect_ratio);
    result.ColumnMajor[5] = 1.0f / tan_part;
    result.ColumnMajor[10] = z_far / (z_far - z_near);
    result.ColumnMajor[11] = 1.0f;
    result.ColumnMajor[14] = (-1.0f * z_near * z_far) / (z_far - z_near);
    return result;
}

float DotProduct(__m128 a, __m128 b) {
    static SIMD_ALIGN float answer[4];
    const uint8_t control = 0x01;
    __m128 mulled = _mm_mul_ps(a, b);

    __m128 tmp0 = _mm_movehl_ps(mulled, mulled);
    __m128 tmp1 = _mm_add_ps(mulled, tmp0);
    __m128 tmp2 = _mm_shuffle_ps(tmp1, tmp1, control);
    __m128 result = _mm_add_ps(tmp1, tmp2);

    _mm_store_ps1((float*)answer, result);
    return (answer[0]);
}

Mat4 MultiplyMat4(const Mat4& first, const Mat4& second) {
    Mat4 result;
    __m128 frows[4];
    __m128 scols[4];

    frows[0] = _mm_load_ps((float*)first.ColumnMajor);
    frows[1] = _mm_load_ps((float*)(first.ColumnMajor + 4));
    frows[2] = _mm_load_ps((float*)(first.ColumnMajor + 8));
    frows[3] = _mm_load_ps((float*)(first.ColumnMajor + 12));
    _MM_TRANSPOSE4_PS(frows[0], frows[1], frows[2], frows[3]);

    scols[0] = _mm_load_ps((float*)second.ColumnMajor);
    scols[1] = _mm_load_ps((float*)(second.ColumnMajor + 4));
    scols[2] = _mm_load_ps((float*)(second.ColumnMajor + 8));
    scols[3] = _mm_load_ps((float*)(second.ColumnMajor + 12));

    for (int i = 0; i < 16; ++i) {
        result.ColumnMajor[i] = DotProduct(frows[i / 4], scols[i % 4]);
    }
    result = Transpose(result);
    return (result);
}