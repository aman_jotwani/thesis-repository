#include <math.h>
struct Vector3 
{
    f32 x, y, z;

    Vector3() {x = y = z = 0.0f;}
    Vector3(f32 _x, f32 _y, f32 _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    void Set(f32 _x, f32 _y, f32 _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    Vector3 operator*(f32 scalar)
    {
        Vector3 result(x * scalar, y * scalar, z * scalar);
        return (result);
    }

    void operator*=(f32 scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
    }

    void operator+=(Vector3 other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
    }

    void operator-=(Vector3 other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
    }

    Vector3 operator+(Vector3 other)
    {
        Vector3 result(x + other.x, y + other.y, z + other.z);
        return (result);
    }

    Vector3 operator-(Vector3 other)
    {
        Vector3 result(x - other.x, y - other.y, z - other.z);
        return (result);
    }

    void Normalize() {
        f32 length = (f32) sqrt(x*x + y*y + z*z);
        x /= length;
        y /= length;
        z /= length;
    }
};

Vector3 CrossProduct(Vector3 first, Vector3 second) {
    return Vector3(first.y*second.z - second.y*first.z,
              first.z*second.x - first.x*second.z,
              first.x*second.y - second.x*first.y);
}
