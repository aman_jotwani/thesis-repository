void vs_debug_log(char* format, ...) {
    char buffer[256];
    va_list args;
    va_start(args, format);
    vsnprintf_s(buffer, sizeof(buffer), format, args);
    va_end(args);

    OutputDebugStringA("TANK: ");
    OutputDebugStringA(buffer);
    OutputDebugStringA("\n");
}

struct File_Handle {
    uint32_t ContentSize;
    void* Contents;
};

static uint32_t SafeTruncateUInt64(uint64_t Value) {
    ASSERT(Value <= 0xFFFFFFFF) return ((uint32_t)Value);
}

void Win32FreeFileMemory(void* Memory) {
    if (Memory) {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}

File_Handle Win32ReadEntireFile(const char* Filename) {
    File_Handle Result = {0};
    HANDLE FileHandle = CreateFileA(Filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (FileHandle != INVALID_HANDLE_VALUE) {
        LARGE_INTEGER FileSize;
        if (GetFileSizeEx(FileHandle, &FileSize)) {
            uint32_t FileSize32 = SafeTruncateUInt64(FileSize.QuadPart);
            Result.Contents = VirtualAlloc(0, FileSize32, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
            if (Result.Contents) {
                DWORD BytesRead = 0;
                if (ReadFile(FileHandle, Result.Contents, FileSize32, &BytesRead, 0) && (BytesRead == FileSize32)) {
                    Result.ContentSize = FileSize32;
                } else {
                    Win32FreeFileMemory(Result.Contents);
                    Result.Contents = 0;
                }
            } else {
            }
        } else {
        }
        CloseHandle(FileHandle);
    } else {
    }
    return (Result);
}

// TODO: abstract keyboard input interface, separate out ImGui stuff
LRESULT CALLBACK HandleWindowsMessages(HWND window, UINT message, WPARAM wparam, LPARAM lparam) {
    PAINTSTRUCT ps;
    HDC hdc;
    ImGuiIO& io = ImGui::GetIO();
    switch (message) {
        case WM_LBUTTONDOWN:
            io.MouseDown[0] = true;
            return true;
        case WM_LBUTTONUP:
            io.MouseDown[0] = false;
            return true;
        case WM_RBUTTONDOWN:
            io.MouseDown[1] = true;
            return true;
        case WM_RBUTTONUP:
            io.MouseDown[1] = false;
            return true;
        case WM_MBUTTONDOWN:
            io.MouseDown[2] = true;
            return true;
        case WM_MBUTTONUP:
            io.MouseDown[2] = false;
            return true;
        case WM_MOUSEWHEEL:
            io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wparam) > 0 ? +1.0f : -1.0f;
            return true;
        case WM_MOUSELEAVE:
            globals.cam.firstTime = true;
        case WM_MOUSEMOVE:
            io.MousePos.x = (signed short)(lparam);
            io.MousePos.y = (signed short)(lparam >> 16);

            {
                f32 xoffset = 0.0f, yoffset = 0.0f;
                if(globals.cam.firstTime) {
                    globals.cam.prevMouseX = (s16) lparam;
                    globals.cam.prevMouseY = (s16) (lparam >> 16);
                    globals.cam.firstTime = false;
                } else {
                    s16 curr_mouse_x = (s16) lparam;
                    s16 curr_mouse_y = (s16) (lparam >> 16);

                    xoffset = (f32) (curr_mouse_x - globals.cam.prevMouseX);
                    yoffset = (f32) (globals.cam.prevMouseY - curr_mouse_y);

                    globals.cam.prevMouseX = curr_mouse_x;
                    globals.cam.prevMouseY = curr_mouse_y;
                }

                f32 sensitivity = 0.1f;
                globals.cam.yawDegrees += xoffset*sensitivity;
                globals.cam.pitchDegrees += yoffset*sensitivity;

                if(globals.cam.pitchDegrees > 89.0f) globals.cam.pitchDegrees = 89.0f;
                else if(globals.cam.pitchDegrees < -89.0f) globals.cam.pitchDegrees = -89.0f;

                f32 yaw_in_radians = ToRadians(globals.cam.yawDegrees);
                f32 pitch_in_radians = ToRadians(globals.cam.pitchDegrees);

                globals.cam.forward.Set(cos(yaw_in_radians)*cos(pitch_in_radians),
                                        sin(pitch_in_radians),
                                        sin(yaw_in_radians)*cos(pitch_in_radians));
                globals.cam.forward.Normalize();
                globals.cam.right = CrossProduct(globals.cam.forward, Vector3(0, 1, 0));
            }
            return true;
        case WM_KEYDOWN:
            if (wparam < 256)
                io.KeysDown[wparam] = 1;
            if(wparam == 'W') {
                globals.cam.position += globals.cam.forward*globals.cam.speed;
            } else if(wparam == 'S') {
                globals.cam.position -= globals.cam.forward*globals.cam.speed;
            } else if(wparam == 'D') {
                globals.cam.position += globals.cam.right*globals.cam.speed;
            } else if(wparam == 'A') {
                globals.cam.position -= globals.cam.right*globals.cam.speed;
            }
            return true;
        case WM_KEYUP:
            if (wparam < 256)
                io.KeysDown[wparam] = 0;
            return true;
        case WM_CHAR:
            // You can also use ToAscii()+GetKeyboardState() to retrieve
            // characters.
            if (wparam > 0 && wparam < 0x10000)
                io.AddInputCharacter((unsigned short)wparam);
            return true;

        case WM_PAINT:
            hdc = BeginPaint(window, &ps);
            EndPaint(window, &ps);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        default:
            return DefWindowProc(window, message, wparam, lparam);
    }

    return 0;
}

void SetupWindow(HINSTANCE instance, s32 desired_screen_width,
                 s32 desired_screen_height, int cmd_show) {
    WNDCLASSEX wcex;
    ZeroMemory(&wcex, sizeof(wcex));
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = HandleWindowsMessages;
    wcex.hInstance = instance;
    wcex.lpszClassName = "WindowClass";
    if (!RegisterClassEx(&wcex)) {
        vs_debug_log("window class registration failed. error code -- %d", GetLastError());
    }

    // Create window
    RECT rc = {0, 0, desired_screen_width, desired_screen_height};
    globals.windowHandle = CreateWindow("WindowClass", "Euphoria d3d11", WS_OVERLAPPEDWINDOW,
                                         CW_USEDEFAULT, CW_USEDEFAULT, 
                                         rc.right - rc.left, rc.bottom - rc.top,
                                         NULL, NULL, instance, NULL);
    if (!globals.windowHandle) {
        vs_debug_log("window creation failed. error code -- %d", GetLastError());
        ASSERT(NULL);
    }

    ShowWindow(globals.windowHandle, cmd_show);
}

