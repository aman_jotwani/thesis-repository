// VARIABLE ARGUMENT RECIPE
void
vs_debug_log(char* format, ... )
{
    char buffer[256];
    va_list args;
    va_start(args, format);
    vsnprintf_s(buffer, sizeof(buffer), format, args);
    va_end(args);
    OutputDebugStringA(buffer);
}

// IMGUI
// list of draw commands
        {
            bool s = true;
            // ImGui::ShowTestWindow(&s);
            ImGuiWindowFlags flags = 0;
            ImGui::SetNextWindowSize(ImVec2(400, 200));
            ImGui::Begin("Draw commands", &s, flags);
            ImGui::BeginChild("Cmd List", ImVec2(200, 200));
            static int curr_list_item = 0;
            const char* listbox_items[] = {
                "Object 1", "Object 2", "Object 3", "Object 4", "Object 5",
                "Object 6", "Object 7", "Object 8", "Object 9", "Object 10",
                "Object 11", "Object 12", "Object 13", "Object 14", "Object 15",
                "Object 16", "Object 17", "Object 18", "Object 19", "Object 20",
                "Object 21", "Object 22", "Object 23", "Object 24", "Object 25",
                "Object 26", "Object 27", "Object 28", "Object 29", "Object 30",
                "Object 31", "Object 32", "Object 33", "Object 34", "Object 35",
                "Object 36", "Object 37", "Object 38", "Object 39", "Object 40",
                "Object 41", "Object 42", "Object 43", "Object 44", "Object 45",
                "Object 46", "Object 47", "Object 48", "Object 49", "Object 50",
                "Object 51", "Object 52", "Object 53", "Object 54", "Object 55",
                "Object 56", "Object 57", "Object 58", "Object 59", "Object 60",
                "Object 61", "Object 62", "Object 63", "Object 64", "Object 65",
                "Object 66", "Object 67", "Object 68", "Object 69", "Object 70",
                "Object 71", "Object 72", "Object 73", "Object 74", "Object 75",
                "Object 76", "Object 77", "Object 78", "Object 79", "Object 80",
                "Object 81", "Object 82", "Object 83", "Object 84", "Object 85",
                "Object 86", "Object 87", "Object 88", "Object 89", "Object 90",
                "Object 91", "Object 92", "Object 93", "Object 94", "Object 95",
                "Object 96", "Object 97", "Object 98", "Object 99", "Object 100",
                "Object 101", "Object 102", "Object 103", "Object 104", "Object 105",
                "Object 106", "Object 107", "Object 108", "Object 109", "Object 110",
                "Object 111", "Object 112", "Object 113", "Object 114", "Object 115",
                "Object 116", "Object 117", "Object 118", "Object 119", "Object 120",
                "Object 121", "Object 122", "Object 123", "Object 124", "Object 125",
                "Object 126", "Object 127", "Object 128", "Object 129", "Object 130",
                "Object 131", "Object 132", "Object 133", "Object 134", "Object 135",
                "Object 136", "Object 137", "Object 138", "Object 139", "Object 140",
                "Object 141", "Object 142", "Object 143", "Object 144", "Object 145",
                "Object 146", "Object 147", "Object 148", "Object 149", "Object 150",
                "Object 151", "Object 152", "Object 153", "Object 154", "Object 155",
                "Object 156", "Object 157", "Object 158", "Object 159", "Object 160",
                "Object 161", "Object 162", "Object 163", "Object 164", "Object 165",
                "Object 166", "Object 167", "Object 168", "Object 169", "Object 170",
                "Object 171", "Object 172", "Object 173", "Object 174", "Object 175",
                "Object 176", "Object 177", "Object 178", "Object 179", "Object 180",
                "Object 181", "Object 182", "Object 183", "Object 184", "Object 185",
                "Object 186", "Object 187", "Object 188", "Object 189", "Object 190",
                "Object 191", "Object 192", "Object 193", "Object 194", "Object 195",
                "Object 196", "Object 197", "Object 198", "Object 199", "Object 200",
                "Object 201", "Object 202", "Object 203", "Object 204", "Object 205",
                "Object 206", "Object 207", "Object 208", "Object 209", "Object 210",
                "Object 211", "Object 212", "Object 213", "Object 214", "Object 215",
                "Object 216", "Object 217", "Object 218", "Object 219", "Object 220",
                "Object 221", "Object 222", "Object 223", "Object 224", "Object 225",
                "Object 226", "Object 227", "Object 228", "Object 229", "Object 230",
                "Object 231", "Object 232", "Object 233", "Object 234", "Object 235",
                "Object 236", "Object 237", "Object 238", "Object 239", "Object 240",
                "Object 241", "Object 242", "Object 243", "Object 244", "Object 245",
                "Object 246" , "Object 247", "Object 248", "Object 249", "Object 250",
                "Object 251" , "Object 252", "Object 253", "Object 254", "Object 255",
                "Object 256" , "Object 257", "Object 258", "Object 259", "Object 260",
                "Object 261" , "Object 262", "Object 263", "Object 264", "Object 265",
                "Object 266" , "Object 267", "Object 268", "Object 269", "Object 270",
                "Object 271" , "Object 272", "Object 273", "Object 274", "Object 275",
                "Object 276" , "Object 277", "Object 278", "Object 279", "Object 280",
                "Object 281" , "Object 282", "Object 283", "Object 284", "Object 285",
                "Object 286" , "Object 287", "Object 288", "Object 289", "Object 290",
                "Object 291" , "Object 292", "Object 293", "Object 294", "Object 295",
                "Object 296" , "Object 297", "Object 298", "Object 299", "Object 300",
                "Object 301" , "Object 302", "Object 303", "Object 304", "Object 305",
                "Object 306" , "Object 307", "Object 308", "Object 309", "Object 310",
                "Object 311" , "Object 312", "Object 313", "Object 314", "Object 315",
                "Object 316" , "Object 317", "Object 318", "Object 319", "Object 320",
                "Object 321" , "Object 322", "Object 323", "Object 324", "Object 325",
                "Object 326" , "Object 327", "Object 328", "Object 329", "Object 330",
                "Object 331" , "Object 332", "Object 333", "Object 334", "Object 335",
                "Object 336" , "Object 337", "Object 338", "Object 339", "Object 340",
                "Object 341" , "Object 342", "Object 343", "Object 344", "Object 345",
                "Object 346" , "Object 347", "Object 348", "Object 349", "Object 350",
                "Object 351" , "Object 352", "Object 353", "Object 354", "Object 355",
                "Object 356" , "Object 357", "Object 358", "Object 359", "Object 360",
                "Object 361" , "Object 362", "Object 363", "Object 364", "Object 365",
                "Object 366" , "Object 367", "Object 368", "Object 369", "Object 370",
                "Object 371" , "Object 372", "Object 373", "Object 374", "Object 375",
                "Object 376" , "Object 377", "Object 378", "Object 379", "Object 380",
                "Object 381" , "Object 382", "Object 383", "Object 384", "Object 385",
                "Object 386" , "Object 387", "Object 388", "Object 389", "Object 390",
                "Object 391" , "Object 392", "Object 393", "Object 394", "Object 395",
                "Object 396" , "Object 397", "Object 398", "Object 399", "Object 400",
                "Object 401" , "Object 402", "Object 403", "Object 404", "Object 405",
                "Object 406" , "Object 407", "Object 408", "Object 409", "Object 410",
                "Object 411" , "Object 412", "Object 413", "Object 414", "Object 415",
                "Object 416" , "Object 417", "Object 418", "Object 419", "Object 420",
                "Object 421" , "Object 422", "Object 423", "Object 424", "Object 425",
                "Object 426" , "Object 427", "Object 428", "Object 429", "Object 430",
                "Object 431" , "Object 432", "Object 433", "Object 434", "Object 435",
                "Object 436" , "Object 437", "Object 438", "Object 439", "Object 440",
                "Object 441" , "Object 442", "Object 443", "Object 444", "Object 445",
            };
            ASSERT(ARRAYSIZE(listbox_items) >= render_list.cmdList.size);
            ImGui::ListBox("", &curr_list_item, listbox_items, render_list.cmdList.size, 20);
            ImGui::EndChild();
            ImGui::SameLine();
            ImGui::PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 5.0f);
            ImGui::BeginChild("Cmd_Description", ImVec2(200, 200));
            Draw_Cmd* selected_cmd = &render_list.cmdList[curr_list_item];
            ImGui::Checkbox("Active", &selected_cmd->active);
            ImGui::Text("Start Index: %d", selected_cmd->startIdx);
            ImGui::Text("Index Count: %d", selected_cmd->numIdxs);
            // Material selected_mat = render_list.matList[selected_cmd->matIdx];
            // ImGui::Text("Material name: %s", selected_mat.name);
            // ImGui::Image((void*) selected_mat.diffuseMap.ptr, ImVec2(selected_mat.diffuseMap.width, selected_mat.diffuseMap.height));
            ImGui::EndChild();
            ImGui::PopStyleVar();
            ImGui::End();
        }
