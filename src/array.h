// completely taken from ImGui::ImVector
template <typename T> struct Array {
    int size;
    int capacity;
    T* data;

    Array() {
        size = capacity = 0;
        data = NULL;
    }

    T& operator[](int i) {
        ASSERT(i < size);
        return data[i];
    }

    const T& operator[](int i) const {
        ASSERT(i < size);
        return data[i];
    }

    void push_back(T val) {
        if (size == capacity)
            reserve(_grow_capacity(size + 1));
        data[size++] = val;
    }

    int _grow_capacity(int new_size) {
        // increase by 50%
        int new_capacity = capacity ? (capacity + capacity / 2) : 8;
        int result = new_capacity > new_size ? new_capacity : new_size;
        return result;
    }

    void reserve(int new_capacity) {
        if (new_capacity <= capacity)
            return;
        T* new_data = new T[new_capacity];
        if (data)
            memcpy(new_data, data, (size_t)size * sizeof(T));
        delete data;
        data = new_data;
        capacity = new_capacity;
    }

    void extend(Array other) {
        for (int i = 0; i < other.size; ++i) {
            push_back(other[i]);
        }
    }

    void free() {
        if (data)
            delete data;
    }
};

typedef Array<char> String;